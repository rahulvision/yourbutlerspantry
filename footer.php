<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package YourButlersPantry
 */

?>
	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="footer-icon-section">
			<div class="footer-icon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/footer-icon.png" /></div>
		</div>
		<div class="container container-footer">
		<?php 	if ( is_active_sidebar( 'ybp-footer-col-4' ) ) {
					$widget_columns = 4;
				} else if ( is_active_sidebar( 'ybp-footer-col-3' ) ) {
					$widget_columns = 3;
				} else if ( is_active_sidebar( 'ybp-footer-col-2' ) ) {
					$widget_columns = 2;
				} else if ( is_active_sidebar( 'ybp-footer-col-1' ) ) {
					$widget_columns = 1;
				} else {
					$widget_columns = 0;
				}
				if ( $widget_columns > 0 ) : ?>
					<div class="footer-top">
					<?php
						$i = 0;
						while ( $i < $widget_columns ) : $i++;
							if ( is_active_sidebar( 'ybp-footer-col-' . $i ) ) : ?>
								<div class="col-<?php echo intval( $widget_columns ); ?>">
									<?php dynamic_sidebar( 'ybp-footer-col-' . intval( $i ) ); ?>
								</div>
						<?php
							endif;
						endwhile;
					?>
					</div>
			<?php endif; ?>
			<?php if ( is_active_sidebar( 'ybp-footer-copyright' ) ) : ?>
						<div class="footer-copyright">
							<?php dynamic_sidebar( 'ybp-footer-copyright' ); ?>
						</div>
					<?php
						endif;
					?>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->
	<?php if(is_page('services') || is_404() ){ ?>
		<script>				
			jQuery('.header-icon-section .header-icon img').css('top', "8px");				
		</script>
	<?php }	 ?>

<?php wp_footer(); ?>

</body>
</html>
