<?php
/**
 * Register widget with WordPress.
 */
function yourbutlerspantry_social_media_widget_load() {
    register_widget( 'yourbutlerspantry_social_media_widget' );
}
add_action( 'widgets_init', 'yourbutlerspantry_social_media_widget_load' );

/**
 * Adds yourbutlerspantry_social_media_widget widget.
 */
class yourbutlerspantry_social_media_widget extends WP_Widget {
	function __construct() {
		parent::__construct(
			// Base ID of your widget
			'ybp-social-media-widget',
			esc_html__('YBP Social Media', 'yourbutlerspantry'),
			array( 'description' => esc_html__( 'Social Media on the footer', 'yourbutlerspantry' ), )
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		$ybp_title    	     = $instance['ybp_title'];
		$ybp_facebook_url    = $instance['ybp_facebook_url'];
		$ybp_pinterest_url   = $instance['ybp_pinterest_url'];
		$ybp_twitter_url     = $instance['ybp_twitter_url'];
		$ybp_google_url      = $instance['ybp_google_url'];
		$ybp_linkedin_url    = $instance['ybp_linkedin_url'];
		$ybp_youtube_url     = $instance['ybp_youtube_url'];
		$ybp_instagram_url   = $instance['ybp_instagram_url'];
		$ybp_rss_url         = $instance['ybp_rss_url'];
	
		echo $args['before_widget'];
		if ( ! empty( $ybp_title ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $ybp_title ) . $args['after_title'];
		}

		echo "<ul class='ybp-social-media'>";
			if ( $ybp_instagram_url ) {
				echo "<li><a href='$ybp_instagram_url' target='_blank' class='social-instagram'><i class='fab fa-instagram'></i></a></li>";
			}
			if ( $ybp_pinterest_url ) {
				echo "<li><a href='$ybp_pinterest_url' target='_blank' class='social-pinterest'><i class='fab fa-pinterest-p'></i></a></li>";
			}
			if ( $ybp_facebook_url ) {
				echo "<li><a href='$ybp_facebook_url' target='_blank' class='social-facebook'><i class='fab fa-facebook-f'></i></a></li>";
			}			
			if ( $ybp_twitter_url ) {
				echo "<li><a href='$ybp_twitter_url' target='_blank' class='social-twitter'><i class='fab fa-twitter'></i></a></li>";
			}
			if ( $ybp_google_url ) {
				echo "<li><a href='$ybp_google_url' target='_blank' class='social-google'><i class='fab fa-google-plus-g'></i></a></li>";
			}
			if ( $ybp_linkedin_url ) {
				echo "<li><a href='$ybp_linkedin_url' target='_blank' class='social-linkedin'><i class='fab fa-linkedin'></i></a></li>";
			}
			if ( $ybp_youtube_url ) {
				echo "<li><a href='$ybp_youtube_url' target='_blank' class='social-youtube'><i class='fab fa-youtube'></i></a></li>";
			}
			if ( $ybp_rss_url ) {
				echo "<li><a href='$ybp_rss_url' target='_blank' class='social-rss'><i class='fas fa-rss'></i></a></li>";
			}
		echo "</ul>";

		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		if ( isset( $instance[ 'ybp_title' ] ) ) {
			$ybp_title = $instance[ 'ybp_title' ];
		}
		if ( isset( $instance[ 'ybp_facebook_url' ] ) ) {
			$ybp_facebook_url = $instance[ 'ybp_facebook_url' ];
		}
		if ( isset( $instance[ 'ybp_pinterest_url' ] ) ) {
			$ybp_pinterest_url = $instance[ 'ybp_pinterest_url' ];
		}
		if ( isset( $instance[ 'ybp_twitter_url' ] ) ) {
			$ybp_twitter_url = $instance[ 'ybp_twitter_url' ];
		}
		if ( isset( $instance[ 'ybp_google_url' ] ) ) {
			$ybp_google_url = $instance[ 'ybp_google_url' ];
		}
		if ( isset( $instance[ 'ybp_linkedin_url' ] ) ) {
			$ybp_linkedin_url = $instance[ 'ybp_linkedin_url' ];
		}
		if ( isset( $instance[ 'ybp_youtube_url' ] ) ) {
			$ybp_youtube_url = $instance[ 'ybp_youtube_url' ];
		}
		if ( isset( $instance[ 'ybp_instagram_url' ] ) ) {
			$ybp_instagram_url = $instance[ 'ybp_instagram_url' ];
		}
		if ( isset( $instance[ 'ybp_rss_url' ] ) ) {
			$ybp_rss_url = $instance[ 'ybp_rss_url' ];
		}
	?>
		<p>
			<label for="<?php echo $this->get_field_id( 'ybp_title' ); ?>"><?php _e( 'Title:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'ybp_title' ); ?>" name="<?php echo $this->get_field_name( 'ybp_title' ); ?>" type="text" value="<?php echo esc_attr( $ybp_title ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'ybp_facebook_url' ); ?>"><?php _e( 'Facebook URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'ybp_facebook_url' ); ?>" name="<?php echo $this->get_field_name( 'ybp_facebook_url' ); ?>" type="text" value="<?php echo esc_attr( $ybp_facebook_url ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'ybp_pinterest_url' ); ?>"><?php _e( 'Pinterest URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'ybp_pinterest_url' ); ?>" name="<?php echo $this->get_field_name( 'ybp_pinterest_url' ); ?>" type="text" value="<?php echo esc_attr( $ybp_pinterest_url ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'ybp_twitter_url' ); ?>"><?php _e( 'Twitter URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'ybp_twitter_url' ); ?>" name="<?php echo $this->get_field_name( 'ybp_twitter_url' ); ?>" type="text" value="<?php echo esc_attr( $ybp_twitter_url ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'ybp_google_url' ); ?>"><?php _e( 'Google Plus URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'ybp_google_url' ); ?>" name="<?php echo $this->get_field_name( 'ybp_google_url' ); ?>" type="text" value="<?php echo esc_attr( $ybp_google_url ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'ybp_linkedin_url' ); ?>"><?php _e( 'Linkedin URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'ybp_linkedin_url' ); ?>" name="<?php echo $this->get_field_name( 'ybp_linkedin_url' ); ?>" type="text" value="<?php echo esc_attr( $ybp_linkedin_url ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'ybp_youtube_url' ); ?>"><?php _e( 'Youtube URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'ybp_youtube_url' ); ?>" name="<?php echo $this->get_field_name( 'ybp_youtube_url' ); ?>" type="text" value="<?php echo esc_attr( $ybp_youtube_url ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'ybp_instagram_url' ); ?>"><?php _e( 'Instagram URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'ybp_instagram_url' ); ?>" name="<?php echo $this->get_field_name( 'ybp_instagram_url' ); ?>" type="text" value="<?php echo esc_attr( $ybp_instagram_url ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'ybp_rss_url' ); ?>"><?php _e( 'RSS URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'ybp_rss_url' ); ?>" name="<?php echo $this->get_field_name( 'ybp_rss_url' ); ?>" type="text" value="<?php echo esc_attr( $ybp_rss_url ); ?>" />
		</p>
	<?php
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {

		return $new_instance;
	}
}