<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package yourbutlerspantry
 */

/**
 * Register widget with WordPress.
 */
function ybp_contact_widget_load() {
    register_widget( 'ybp_contact_widget' );
}
add_action( 'widgets_init', 'ybp_contact_widget_load' );

/**
 * Adds ybp_contact_widget widget.
 */
class ybp_contact_widget extends WP_Widget {
	function __construct() {
		parent::__construct(
			// Base ID of your widget
			'ybp-contact-widget',
			esc_html__('YBP Contact', 'yourbutlerspantry'),
			array( 'description' => esc_html__( 'Contact Details on the footer', 'yourbutlerspantry' ), )
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		$ybp_contact_title     = $instance['ybp_contact_title'];
		$ybp_contact_address   = $instance['ybp_contact_address'];
		$ybp_contact_phone     = $instance['ybp_contact_phone'];
		$ybp_contact_sec_phone = $instance['ybp_contact_sec_phone'];
		$ybp_contact_email     = $instance['ybp_contact_email'];
		$ybp_contact_link      = $instance['ybp_contact_link'];

		echo $args['before_widget'];
		if ( ! empty( $ybp_contact_title ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $ybp_contact_title ) . $args['after_title'];
		}

		echo "<ul class='ybp-contact-details'>";
			if($ybp_contact_phone){
				echo "<li class='contact-phone'><a href='tel:". $ybp_contact_phone ."'>". $ybp_contact_phone ."</a></li>";
			}
			if($ybp_contact_sec_phone){
				echo "<li><a href='tel:". $ybp_contact_sec_phone ."'>". $ybp_contact_sec_phone ."</a></li>";
			}
			if($ybp_contact_email){
				echo "<li><a href='mailto:". $ybp_contact_email ."'>". $ybp_contact_email ."</a></li>";
			}
			if($ybp_contact_address){
				echo "<li>". $ybp_contact_address ."</li>";
			}
			if($ybp_contact_link){
				echo "<li><a href='". $ybp_contact_link ."'>".esc_html__( 'Learn More', 'yourbutlerspantry' )."</a></li>";
			}
		echo "</ul>";
		echo $args['after_widget'];
	}

	/**
	* Back-end widget form.
	*
	* @see WP_Widget::form()
	*
	* @param array $instance Previously saved values from database.
	*/
	public function form( $instance ) {

		if ( isset( $instance[ 'ybp_contact_title' ] ) ) {
			$ybp_contact_title = $instance[ 'ybp_contact_title' ];
		}
		if ( isset( $instance[ 'ybp_contact_address' ] ) ) {
			$ybp_contact_address = $instance[ 'ybp_contact_address' ];
		}
		if ( isset( $instance[ 'ybp_contact_phone' ] ) ) {
			$ybp_contact_phone = $instance[ 'ybp_contact_phone' ];
		}
		if ( isset( $instance[ 'ybp_contact_sec_phone' ] ) ) {
			$ybp_contact_sec_phone = $instance[ 'ybp_contact_sec_phone' ];
		}
		if ( isset( $instance[ 'ybp_contact_email' ] ) ) {
			$ybp_contact_email = $instance[ 'ybp_contact_email' ];
		}
		if ( isset( $instance[ 'ybp_contact_link' ] ) ) {
			$ybp_contact_link = $instance[ 'ybp_contact_link' ];
		}
	?>
		<p>
			<label for="<?php echo $this->get_field_id( 'ybp_contact_title' ); ?>"><?php _e( 'Contact Title:', 'yourbutlerspantry' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'ybp_contact_title' ); ?>" name="<?php echo $this->get_field_name( 'ybp_contact_title' ); ?>" type="text" value="<?php echo esc_attr( $ybp_contact_title ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'ybp_contact_address' ); ?>"><?php _e( 'Address:', 'yourbutlerspantry' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'ybp_contact_address' ); ?>" name="<?php echo $this->get_field_name( 'ybp_contact_address' ); ?>" type="text" value="<?php echo esc_attr( $ybp_contact_address ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'ybp_contact_phone' ); ?>"><?php _e( 'Phone:', 'yourbutlerspantry' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'ybp_contact_phone' ); ?>" name="<?php echo $this->get_field_name( 'ybp_contact_phone' ); ?>" type="text" value="<?php echo esc_attr( $ybp_contact_phone ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'ybp_contact_sec_phone' ); ?>"><?php _e( 'Secondary Phone:', 'yourbutlerspantry' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'ybp_contact_sec_phone' ); ?>" name="<?php echo $this->get_field_name( 'ybp_contact_sec_phone' ); ?>" type="text" value="<?php echo esc_attr( $ybp_contact_sec_phone ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'ybp_contact_email' ); ?>"><?php _e( 'Email:', 'yourbutlerspantry' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'ybp_contact_email' ); ?>" name="<?php echo $this->get_field_name( 'ybp_contact_email' ); ?>" type="text" value="<?php echo esc_attr( $ybp_contact_email ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'ybp_contact_link' ); ?>"><?php _e( 'Link:', 'yourbutlerspantry' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'ybp_contact_link' ); ?>" name="<?php echo $this->get_field_name( 'ybp_contact_link' ); ?>" type="text" value="<?php echo esc_attr( $ybp_contact_link ); ?>" />
		</p>
	<?php
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {		
		return $new_instance;
	}
}