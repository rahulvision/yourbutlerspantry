<?php
// Register Our People Type
function ybp_our_people() {

	$labels = array(
		'name'                  => _x( 'Our People', 'Post Type General Name', 'yourbutlerspantry' ),
		'singular_name'         => _x( 'Our People', 'Post Type Singular Name', 'yourbutlerspantry' ),
		'menu_name'             => __( 'Our People', 'yourbutlerspantry' ),
		'name_admin_bar'        => __( 'Our People', 'yourbutlerspantry' ),
		'archives'              => __( 'Item Archives', 'yourbutlerspantry' ),
		'attributes'            => __( 'Item Attributes', 'yourbutlerspantry' ),
		'parent_item_colon'     => __( 'Parent Item:', 'yourbutlerspantry' ),
		'all_items'             => __( 'All Peoples', 'yourbutlerspantry' ),
		'add_new_item'          => __( 'Add New People', 'yourbutlerspantry' ),
		'add_new'               => __( 'Add New', 'yourbutlerspantry' ),
		'new_item'              => __( 'New People', 'yourbutlerspantry' ),
		'edit_item'             => __( 'Edit People', 'yourbutlerspantry' ),
		'update_item'           => __( 'Update People', 'yourbutlerspantry' ),
		'view_item'             => __( 'View People', 'yourbutlerspantry' ),
		'view_items'            => __( 'View People', 'yourbutlerspantry' ),
		'search_items'          => __( 'Search People', 'yourbutlerspantry' ),
		'not_found'             => __( 'Not found', 'yourbutlerspantry' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'yourbutlerspantry' ),
		'featured_image'        => __( 'Featured Image', 'yourbutlerspantry' ),
		'set_featured_image'    => __( 'Set featured image', 'yourbutlerspantry' ),
		'remove_featured_image' => __( 'Remove featured image', 'yourbutlerspantry' ),
		'use_featured_image'    => __( 'Use as featured image', 'yourbutlerspantry' ),
		'insert_into_item'      => __( 'Insert into item', 'yourbutlerspantry' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'yourbutlerspantry' ),
		'items_list'            => __( 'Items list', 'yourbutlerspantry' ),
		'items_list_navigation' => __( 'Items list navigation', 'yourbutlerspantry' ),
		'filter_items_list'     => __( 'Filter items list', 'yourbutlerspantry' ),
	);
	$args = array(
		'label'                 => __( 'Our People', 'yourbutlerspantry' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-groups',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'ourpeople', $args );

}
add_action( 'init', 'ybp_our_people', 0 );

// Register Testimonial Post Type
function ybp_testimonial() {

	$labels = array(
		'name'                  => _x( 'Testimonial', 'Post Type General Name', 'yourbutlerspantry' ),
		'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name', 'yourbutlerspantry' ),
		'menu_name'             => __( 'Testimonial', 'yourbutlerspantry' ),
		'name_admin_bar'        => __( 'Testimonial', 'yourbutlerspantry' ),
		'archives'              => __( 'Item Archives', 'yourbutlerspantry' ),
		'attributes'            => __( 'Item Attributes', 'yourbutlerspantry' ),
		'parent_item_colon'     => __( 'Parent Testimonial:', 'yourbutlerspantry' ),
		'all_items'             => __( 'All Testimonials', 'yourbutlerspantry' ),
		'add_new_item'          => __( 'Add New Testimonial', 'yourbutlerspantry' ),
		'add_new'               => __( 'Add New', 'yourbutlerspantry' ),
		'new_item'              => __( 'New Testimonial', 'yourbutlerspantry' ),
		'edit_item'             => __( 'Edit Testimonial', 'yourbutlerspantry' ),
		'update_item'           => __( 'Update Testimonial', 'yourbutlerspantry' ),
		'view_item'             => __( 'View Testimonial', 'yourbutlerspantry' ),
		'view_items'            => __( 'View Testimonial', 'yourbutlerspantry' ),
		'search_items'          => __( 'Search Testimonial', 'yourbutlerspantry' ),
		'not_found'             => __( 'Not found', 'yourbutlerspantry' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'yourbutlerspantry' ),
		'featured_image'        => __( 'Featured Image', 'yourbutlerspantry' ),
		'set_featured_image'    => __( 'Set featured image', 'yourbutlerspantry' ),
		'remove_featured_image' => __( 'Remove featured image', 'yourbutlerspantry' ),
		'use_featured_image'    => __( 'Use as featured image', 'yourbutlerspantry' ),
		'insert_into_item'      => __( 'Insert into Testimonial', 'yourbutlerspantry' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Testimonial', 'yourbutlerspantry' ),
		'items_list'            => __( 'Items list', 'yourbutlerspantry' ),
		'items_list_navigation' => __( 'Items list navigation', 'yourbutlerspantry' ),
		'filter_items_list'     => __( 'Filter items list', 'yourbutlerspantry' ),
	);
	$args = array(
		'label'                 => __( 'Testimonial', 'yourbutlerspantry' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-admin-users',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'ybp-testimonial', $args );

}
add_action( 'init', 'ybp_testimonial', 0 );

// Register QA Custom Post Type
function ybp_faq() {

	$labels = array(
		'name'                  => _x( 'QA', 'Post Type General Name', 'yourbutlerspantry' ),
		'singular_name'         => _x( 'QA', 'Post Type Singular Name', 'yourbutlerspantry' ),
		'menu_name'             => __( 'QA', 'yourbutlerspantry' ),
		'name_admin_bar'        => __( 'QA', 'yourbutlerspantry' ),
		'archives'              => __( 'QA Archives', 'yourbutlerspantry' ),
		'attributes'            => __( 'QA Attributes', 'yourbutlerspantry' ),
		'parent_item_colon'     => __( 'Parent QA:', 'yourbutlerspantry' ),
		'all_items'             => __( 'All QA', 'yourbutlerspantry' ),
		'add_new_item'          => __( 'Add New QA', 'yourbutlerspantry' ),
		'add_new'               => __( 'Add New', 'yourbutlerspantry' ),
		'new_item'              => __( 'New QA', 'yourbutlerspantry' ),
		'edit_item'             => __( 'Edit QA', 'yourbutlerspantry' ),
		'update_item'           => __( 'Update QA', 'yourbutlerspantry' ),
		'view_item'             => __( 'View QA', 'yourbutlerspantry' ),
		'view_items'            => __( 'View QA', 'yourbutlerspantry' ),
		'search_items'          => __( 'Search QA', 'yourbutlerspantry' ),
		'not_found'             => __( 'Not found', 'yourbutlerspantry' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'yourbutlerspantry' ),
		'featured_image'        => __( 'Featured Image', 'yourbutlerspantry' ),
		'set_featured_image'    => __( 'Set featured image', 'yourbutlerspantry' ),
		'remove_featured_image' => __( 'Remove featured image', 'yourbutlerspantry' ),
		'use_featured_image'    => __( 'Use as featured image', 'yourbutlerspantry' ),
		'insert_into_item'      => __( 'Insert into QA', 'yourbutlerspantry' ),
		'uploaded_to_this_item' => __( 'Uploaded to this QA', 'yourbutlerspantry' ),
		'items_list'            => __( 'Items list', 'yourbutlerspantry' ),
		'items_list_navigation' => __( 'Items list navigation', 'yourbutlerspantry' ),
		'filter_items_list'     => __( 'Filter items list', 'yourbutlerspantry' ),
	);
	$args = array(
		'label'                 => __( 'QA', 'yourbutlerspantry' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-format-chat',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'ybpfaq', $args );

}
add_action( 'init', 'ybp_faq', 0 );

// Register QA Custom Taxonomy
function ybp_faq_category() {
	
	$labels = array(
		'name'                       => _x( 'QA Category', 'Taxonomy General Name', 'yourbutlerspantry' ),
		'singular_name'              => _x( 'QA Category', 'Taxonomy Singular Name', 'yourbutlerspantry' ),
		'menu_name'                  => __( 'QA Category', 'yourbutlerspantry' ),
		'all_items'                  => __( 'All Category', 'yourbutlerspantry' ),
		'parent_item'                => __( 'Parent Category', 'yourbutlerspantry' ),
		'parent_item_colon'          => __( 'Parent Category:', 'yourbutlerspantry' ),
		'new_item_name'              => __( 'New Category Name', 'yourbutlerspantry' ),
		'add_new_item'               => __( 'Add New Category', 'yourbutlerspantry' ),
		'edit_item'                  => __( 'Edit Category', 'yourbutlerspantry' ),
		'update_item'                => __( 'Update Category', 'yourbutlerspantry' ),
		'view_item'                  => __( 'View Category', 'yourbutlerspantry' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'yourbutlerspantry' ),
		'add_or_remove_items'        => __( 'Add or remove Category', 'yourbutlerspantry' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'yourbutlerspantry' ),
		'popular_items'              => __( 'Popular Category', 'yourbutlerspantry' ),
		'search_items'               => __( 'Search Category', 'yourbutlerspantry' ),
		'not_found'                  => __( 'Not Found', 'yourbutlerspantry' ),
		'no_terms'                   => __( 'No Category', 'yourbutlerspantry' ),
		'items_list'                 => __( 'Category list', 'yourbutlerspantry' ),
		'items_list_navigation'      => __( 'Category list navigation', 'yourbutlerspantry' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
	);
	register_taxonomy( 'ybp-faq-category', array( 'ybpfaq' ), $args );

}
add_action( 'init', 'ybp_faq_category', 0 );

// Register Rentals Post Type
function ybp_rentals() {

	$labels = array(
		'name'                  => _x( 'Rentals', 'Post Type General Name', 'yourbutlerspantry' ),
		'singular_name'         => _x( 'Rentals', 'Post Type Singular Name', 'yourbutlerspantry' ),
		'menu_name'             => __( 'Rentals', 'yourbutlerspantry' ),
		'name_admin_bar'        => __( 'Rentals', 'yourbutlerspantry' ),
		'archives'              => __( 'Rentals Archives', 'yourbutlerspantry' ),
		'attributes'            => __( 'Rentals Attributes', 'yourbutlerspantry' ),
		'parent_item_colon'     => __( 'Parent Item:', 'yourbutlerspantry' ),
		'all_items'             => __( 'All Items', 'yourbutlerspantry' ),
		'add_new_item'          => __( 'Add New Rentals', 'yourbutlerspantry' ),
		'add_new'               => __( 'Add New', 'yourbutlerspantry' ),
		'new_item'              => __( 'New Rentals', 'yourbutlerspantry' ),
		'edit_item'             => __( 'Edit Rentals', 'yourbutlerspantry' ),
		'update_item'           => __( 'Update Rentals', 'yourbutlerspantry' ),
		'view_item'             => __( 'View Rentals', 'yourbutlerspantry' ),
		'view_items'            => __( 'View Rentals', 'yourbutlerspantry' ),
		'search_items'          => __( 'Search Rentals', 'yourbutlerspantry' ),
		'not_found'             => __( 'Not found', 'yourbutlerspantry' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'yourbutlerspantry' ),
		'featured_image'        => __( 'Featured Image', 'yourbutlerspantry' ),
		'set_featured_image'    => __( 'Set featured image', 'yourbutlerspantry' ),
		'remove_featured_image' => __( 'Remove featured image', 'yourbutlerspantry' ),
		'use_featured_image'    => __( 'Use as featured image', 'yourbutlerspantry' ),
		'insert_into_item'      => __( 'Insert into Rentals', 'yourbutlerspantry' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Rentals', 'yourbutlerspantry' ),
		'items_list'            => __( 'Items list', 'yourbutlerspantry' ),
		'items_list_navigation' => __( 'Items list navigation', 'yourbutlerspantry' ),
		'filter_items_list'     => __( 'Filter items list', 'yourbutlerspantry' ),
	);
	$args = array(
		'label'                 => __( 'Rentals', 'yourbutlerspantry' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'ybprentals', $args );

}
add_action( 'init', 'ybp_rentals', 0 );

// Register Rentals Taxonomy
function ybp_rentals_category() {

	$labels = array(
		'name'                       => _x( 'Rentals Category', 'Taxonomy General Name', 'yourbutlerspantry' ),
		'singular_name'              => _x( 'Rentals Category', 'Taxonomy Singular Name', 'yourbutlerspantry' ),
		'menu_name'                  => __( 'Rentals Category', 'yourbutlerspantry' ),
		'all_items'                  => __( 'All Category', 'yourbutlerspantry' ),
		'parent_item'                => __( 'Parent Category', 'yourbutlerspantry' ),
		'parent_item_colon'          => __( 'Parent Item:', 'yourbutlerspantry' ),
		'new_item_name'              => __( 'New CategoryName', 'yourbutlerspantry' ),
		'add_new_item'               => __( 'Add New Category', 'yourbutlerspantry' ),
		'edit_item'                  => __( 'Edit Category', 'yourbutlerspantry' ),
		'update_item'                => __( 'Update Category', 'yourbutlerspantry' ),
		'view_item'                  => __( 'View Category', 'yourbutlerspantry' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'yourbutlerspantry' ),
		'add_or_remove_items'        => __( 'Add or remove Category', 'yourbutlerspantry' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'yourbutlerspantry' ),
		'popular_items'              => __( 'Popular Category', 'yourbutlerspantry' ),
		'search_items'               => __( 'Search Category', 'yourbutlerspantry' ),
		'not_found'                  => __( 'Not Found', 'yourbutlerspantry' ),
		'no_terms'                   => __( 'No Category', 'yourbutlerspantry' ),
		'items_list'                 => __( 'Category list', 'yourbutlerspantry' ),
		'items_list_navigation'      => __( 'Category list navigation', 'yourbutlerspantry' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
	);
	register_taxonomy( 'ybp-rentals-category', array( 'ybprentals' ), $args );

}
add_action( 'init', 'ybp_rentals_category', 0 );



// Register Community Partners Custom Post Type
function ybp_community_partners() {

	$labels = array(
		'name'                  => _x( 'Community Partners', 'Post Type General Name', 'yourbutlerspantry' ),
		'singular_name'         => _x( 'Community Partners', 'Post Type Singular Name', 'yourbutlerspantry' ),
		'menu_name'             => __( 'Community Partners', 'yourbutlerspantry' ),
		'name_admin_bar'        => __( 'Community Partners', 'yourbutlerspantry' ),
		'archives'              => __( 'Community Partners Archives', 'yourbutlerspantry' ),
		'attributes'            => __( 'Community Partners Attributes', 'yourbutlerspantry' ),
		'parent_item_colon'     => __( 'Parent Community Partners:', 'yourbutlerspantry' ),
		'all_items'             => __( 'All Community Partners', 'yourbutlerspantry' ),
		'add_new_item'          => __( 'Add New', 'yourbutlerspantry' ),
		'add_new'               => __( 'Add New', 'yourbutlerspantry' ),
		'new_item'              => __( 'New Community Partners', 'yourbutlerspantry' ),
		'edit_item'             => __( 'Edit Community Partners', 'yourbutlerspantry' ),
		'update_item'           => __( 'Update Community Partners', 'yourbutlerspantry' ),
		'view_item'             => __( 'View Community Partners', 'yourbutlerspantry' ),
		'view_items'            => __( 'View Community Partners', 'yourbutlerspantry' ),
		'search_items'          => __( 'Search Community Partners', 'yourbutlerspantry' ),
		'not_found'             => __( 'Not found', 'yourbutlerspantry' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'yourbutlerspantry' ),
		'featured_image'        => __( 'Featured Image', 'yourbutlerspantry' ),
		'set_featured_image'    => __( 'Set featured image', 'yourbutlerspantry' ),
		'remove_featured_image' => __( 'Remove featured image', 'yourbutlerspantry' ),
		'use_featured_image'    => __( 'Use as featured image', 'yourbutlerspantry' ),
		'insert_into_item'      => __( 'Insert into item', 'yourbutlerspantry' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'yourbutlerspantry' ),
		'items_list'            => __( 'Community Partners list', 'yourbutlerspantry' ),
		'items_list_navigation' => __( 'Items list navigation', 'yourbutlerspantry' ),
		'filter_items_list'     => __( 'Filter items list', 'yourbutlerspantry' ),
	);
	$args = array(
		'label'                 => __( 'Community Partners', 'yourbutlerspantry' ),		
		'labels'                => $labels,
		'supports'              => array( 'title' ),		
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'ybpcommunitypartners', $args );

}
add_action( 'init', 'ybp_community_partners', 0 );

// Register  Community Partners Custom Taxonomy
function ybp_community_partners_category() {

	$labels = array(
		'name'                       => _x( 'Community Partners Category', 'Taxonomy General Name', 'yourbutlerspantry' ),
		'singular_name'              => _x( 'Community Partners Category', 'Taxonomy Singular Name', 'yourbutlerspantry' ),
		'menu_name'                  => __( 'Community Partners Category', 'yourbutlerspantry' ),
		'all_items'                  => __( 'All Category', 'yourbutlerspantry' ),
		'parent_item'                => __( 'Parent Category', 'yourbutlerspantry' ),
		'parent_item_colon'          => __( 'Parent Category:', 'yourbutlerspantry' ),
		'new_item_name'              => __( 'New Category Name', 'yourbutlerspantry' ),
		'add_new_item'               => __( 'Add New Category', 'yourbutlerspantry' ),
		'edit_item'                  => __( 'Edit Category', 'yourbutlerspantry' ),
		'update_item'                => __( 'Update Category', 'yourbutlerspantry' ),
		'view_item'                  => __( 'View Category', 'yourbutlerspantry' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'yourbutlerspantry' ),
		'add_or_remove_items'        => __( 'Add or remove Category', 'yourbutlerspantry' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'yourbutlerspantry' ),
		'popular_items'              => __( 'Popular Category', 'yourbutlerspantry' ),
		'search_items'               => __( 'Search Category', 'yourbutlerspantry' ),
		'not_found'                  => __( 'Not Found', 'yourbutlerspantry' ),
		'no_terms'                   => __( 'No Category', 'yourbutlerspantry' ),
		'items_list'                 => __( 'Category list', 'yourbutlerspantry' ),
		'items_list_navigation'      => __( 'Category list navigation', 'yourbutlerspantry' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
	);
	register_taxonomy( 'community-partners-category', array( 'ybpcommunitypartners' ), $args );

}
add_action( 'init', 'ybp_community_partners_category', 0 );
?>