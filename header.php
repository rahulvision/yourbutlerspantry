<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package YourButlersPantry
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
	<?php
	if ( is_home() ) {
		$page_id = get_option( 'page_for_posts' );
	} else {
		$page_id = get_the_ID();
	}
	$page_container_width = get_field( "page_container_width", $page_id );
	if ( ! empty( $page_container_width ) && $page_container_width != "" ) {
		echo '<style>
		body .container {
			max-width: '.$page_container_width.'px;
		}
		</style>';
	}	
	?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'yourbutlerspantry' ); ?></a>

	<header id="masthead" class="site-header">
		<?php
			$top_header_text = get_field('top_header_text', 'option');
			if ( !empty( $top_header_text ) && $top_header_text !="" ) {
			?>
			<div class="header-top">
				<div class="container">
					<p><?php esc_html_e( $top_header_text, 'yourbutlerspantry' ); ?></p>
				</div>
			</div>
		<?php } ?>
		<div class="header-logo">
			<div class="container">
				<?php
				the_custom_logo();
				if ( is_front_page() && is_home() ) :
					?>
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<?php
				else :
					?>
					<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
					<?php
				endif;
				$yourbutlerspantry_description = get_bloginfo( 'description', 'display' );
				if ( $yourbutlerspantry_description || is_customize_preview() ) :
					?>
					<p class="site-description"><?php echo $yourbutlerspantry_description; /* WPCS: xss ok. */ ?></p>
				<?php endif; ?>
			</div>
		</div>
		<div class="header-menu">
			<div class="header-container">
				<nav id="site-navigation" class="main-navigation">
					<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
						<i class="fas fa-bars"></i>
						<?php //esc_html_e( 'Menu', 'yourbutlerspantry' ); ?>
					</button>
					<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
					) );
					?>
				</nav><!-- #site-navigation -->
				<div class="header-social-menu">
				<?php
					if( is_active_sidebar( 'ybp-social-media' ) ) {
						dynamic_sidebar( 'ybp-social-media' );
					}
				 ?>
				</div>
			</div>
		</div>
		<?php
		if ( ! is_front_page() ) {
			$header_icon = get_field( "header_icon", $page_id );
			if ( empty ( $header_icon ) && $header_icon == "" ) {
				$header_icon = get_stylesheet_directory_uri().'/images/services_logo.png';
			}
			?>
			<div class="header-icon-section">
				<div class="header-icon"><img src="<?php echo $header_icon; ?>" /></div>
			</div>
			<div class="ybp-back-to-home">
				<div class="container">
					<a class="ybp-back-home" href="<?php echo esc_url(site_url()); ?>"><?php esc_html_e( 'back to home', 'yourbutlerspantry' ); ?></a>
				</div>
			</div>
	<?php } ?>
	</header><!-- #masthead -->

	<div id="content" class="site-content <?php if( ! get_post_meta( get_the_ID(), '_wpb_vc_js_status', true ) ) { echo " ybp-content"; } ?>">
