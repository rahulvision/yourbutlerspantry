<?php
/**
 * Template part for displaying rentals
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package YourButlersPantry
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content">
		<?php the_content(); ?>
	</div>

	<div class="section page">
		<!--div class="section work-portfolio whiteBg"-->
		<div class="section">
			<div class="container">
				<div class="content">
					<?php $args = array(
							'taxonomy'	=> 'ybp-rentals-category',
							'orderby' 	=> 'id',
							'order' 	=> 'ASC'
							);
					?>
					<?php $tax_menu_items = get_categories( $args ); ?>
					<div class="bottom-wrapper">
						 <div class="ybp-controls">
							<ul>
								<li type="button" class="control ybp-filter-btn" data-filter="all"><span>All</span></li>
								<?php  foreach ( $tax_menu_items as $tax_menu_item ): ?>
									<li type="button" class="control" data-filter="<?php echo '.'.$tax_menu_item->slug; ?>"><span><?php echo $tax_menu_item->name; ?></span></li>
								<?php endforeach; ?>
							</ul>
						</div>
						<?php
							$args1 = array(
								'post_type' 		=> 'ybprentals',
								'posts_per_page' 	=> '-1',
								'order' 			=> 'ASC',
								'orderby'        	=> 'title',
							);

							$loop = new WP_Query($args1);
							if($loop->have_posts()):
							?>
								<div class="inner-row">
									<div class="ybp-rentals-mixup">
										<?php
											while($loop->have_posts()): $loop->the_post();
												$cat_post =  get_the_terms( $post->ID, 'ybp-rentals-category' );
													if ( has_post_thumbnail() ):
														$feat_image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'ybp-rentals-img');
														$full_image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');
													?>
														<div class="mix ybp-rentals-list <?php if($cat_post): foreach ($cat_post as $catPost){ $class =  $catPost->slug.' '; echo $class; } endif; ?>">
															<div class="ybp-mix-box">
																<div class="image-holder">
																	<a href="<?php echo esc_url($full_image[0]); ?>" class="fancybox"  rel="gallery1" title="<?php esc_html(the_title()); ?>" >
																		<img src="<?php echo esc_url($feat_image[0]); ?>" alt="<?php esc_html(the_title()); ?>" />
																		<i class="fas fa-search-plus"></i>
																	</a>
																</div>
																<div class="title-holder">
																	<?php //$work_title =	mb_strimwidth(get_the_title(), 0, 31, "..."); ?>
																	<h4><?php echo get_the_title(); ?></h4>
																</div>
															</div>
														</div>
												<?php endif; ?>
										<?php endwhile; ?>
										<div class="gap"></div>
									</div>
								</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>

</article><!-- #post-<?php the_ID(); ?> -->