<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package YourButlersPantry
 */

?>


<article id="post-<?php the_ID(); ?>" <?php post_class('post-grid-item'); ?>>		
	<div class="post-content">
       <?php
		if ( has_post_thumbnail()):
			?>
			<div class="post-image">
				<?php yourbutlerspantry_post_thumbnail(); ?>
			</div>
		<?php
		endif; ?>
		<div class="post-text">
			<?php
				if ( is_singular() ) :
					the_title( '<h1 class="entry-title">', '</h1>' );
				else :
					the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				endif;
			?>

			<?php
			if ( ! is_single() ) {

				echo '<p>'.wp_trim_words( get_the_content(), 30, '...' ).'</p>';

			?>
				<div class="post-button">
					<a href="<?php echo esc_url( get_permalink() ); ?>" class="bpr-all-btn" ><?php echo esc_html( 'READ MORE', 'yourbutlerspantry' ); ?></a>
				</div>
				<div class="post-meta">
					<div class="post-date-time">
						<span class="date"><?php the_time('j') ?></span>
						<span class="month"><?php the_time('F') ?></span>
					</div>
				</div>

			<?php } else {

				the_content( sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'yourbutlerspantry' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				) );

			} ?>
		</div>
    </div>
</article><!-- #post-<?php the_ID(); ?> -->