<?php
/*
 * Display pinterest lists
 */
function yourbutlerspantry_pinterest_shortcode( $atts, $content = null, $shortcode_handle = '' ) {
	$default_atts = array(
		'ybp_heading'   => '',
	);

	$atts = shortcode_atts( $default_atts, $atts );

	extract($atts);
	ob_start();

	//delete_transient( 'pinterest_boards' );
	$access_token =  get_field('access_token', 'option');
	$pinterest_boards = get_transient( 'pinterest_boards' );
	if ( empty ( $pinterest_boards ) ) {

		$url		  = 'https://api.pinterest.com/v1/me/boards/?access_token='.$access_token;
		$response 	  = wp_remote_request($url);
		if( is_wp_error( $response ) ) {
			echo $result = $response->get_error_message();
		} else {
			$result = wp_remote_retrieve_body($response);
			$result = json_decode( $result, true );
			set_transient( 'pinterest_boards', $result, MINUTE_IN_SECONDS   );
			$pinterest_boards = get_transient( 'pinterest_boards' );
		}
	}
	?>
		<div class="ybp-pinterest-wrap">
		<?php if ( $pinterest_boards['status'] != "failure" ) { ?>
			<div class="ybp-pinterest-boards">
				<ul>
					<?php foreach ( $pinterest_boards['data'] as $res ) { ?>
						<li class="ybp-board" data-filter=".<?php echo sanitize_title($res['name']); ?>"><span><?php echo esc_html($res['name']); ?></span></li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
			<div class="ybp-pinterest">
				<?php
					//delete_transient( 'pinterest_pins' );
					$pinterest_pins = get_transient( 'pinterest_pins' );
					if ( empty ( $pinterest_pins ) ) {

						$pins_fields   = 'id,url,board,image';
						$pins_limit	   = '99';
						$pin_url 	   = 'https://api.pinterest.com/v1/me/pins/?access_token='.$access_token.'&fields='.$pins_fields.'&limit='.$pins_limit;
						$get_board_res = wp_remote_request($pin_url);
						if( is_wp_error( $get_board_res ) ) {
							echo $result = $get_board_res->get_error_message();
						} else {
							$result = wp_remote_retrieve_body($get_board_res);
							$pinresult = json_decode( $result, true );
							set_transient( 'pinterest_pins', $pinresult, 3 * HOUR_IN_SECONDS );
							$pinterest_pins = get_transient( 'pinterest_pins' );
						}

					}
					if ( $pinterest_pins['status'] != "failure" ) {

						foreach ( $pinterest_pins['data'] as $pinres ) { ?>

						<div class="ybp-pinterest-item post-grid-item <?php echo sanitize_title($pinres['board']['name']);?>">
							<a href="<?php echo esc_url($pinres['url']);?>" target="_blank" >
								<img src="<?php echo $pinres['image']['original']['url'];?>" >
							</a>
						</div>

				<?php 	}
					} ?>
			</div>
		</div>
	<?php
	wp_reset_postdata();

	return ob_get_clean();
}

add_shortcode( 'ybp_pinterest', 'yourbutlerspantry_pinterest_shortcode' );

/*
 * Testimonial Composer Element
 */
$shortcode_fields = array(
		array(
			'type'            => 'textfield',
			'heading'         => esc_html__( 'Heading', 'yourbutlerspantry' ),
			'param_name'      => 'ybp_heading',
			'value'           => '',
			'description'     => esc_html__( 'Enter heading.', 'yourbutlerspantry' ),
			'admin_label'     => true,
		),
	);

/*
 * Params
 */
$params = array(
	"name"                   	=> esc_html__( "Pinterest", 'yourbutlerspantry' ),
	"description"            	=> esc_html__( "Display pinterest boards with pin.", 'yourbutlerspantry' ),
	"base"                   	=> 'ybp_pinterest',
	"class"                  	=> 'ybp_element_wrapper',
	"controls"               	=> 'full',
	"icon"                   	=> get_template_directory_uri() . '/images/pinterest.png',
	'category'               	=> esc_html__( 'YBP Addon', 'yourbutlerspantry' ),
	"show_settings_on_create"	=> true,
	"params"                 	=> $shortcode_fields,
);

vc_map( $params );