<?php
/*
 * Display button lists element
 */
function ybp_button_shortcode( $atts, $content = null, $shortcode_handle = '' ) {
	$default_atts = array(
		'ybp_text'       => 'Learn More',
		'ybp_link'       => '',
		'ybp_alignment'  => 'left',
		'ybp_class'      => '',
	);

	$atts = shortcode_atts( $default_atts, $atts );
	
	extract($atts);
	ob_start();
	$ybp_link = vc_build_link($ybp_link);
	?>
	<div class="ybp-button <?php echo esc_attr($ybp_alignment.' '.$ybp_class); ?>">
		<a href="<?php echo esc_url($ybp_link['url']);?>" target="<?php echo ( strlen( $ybp_link['target'] ) > 0 ? esc_attr( $ybp_link['target'] ) : '_self' ); ?>" title="<?php echo esc_attr($ybp_link['title']);?>" rel="<?php echo esc_attr($ybp_link['rel']);?>" class="bpr-all-btn"><?php echo esc_html($ybp_text);?></a>
	</div>
	<?php
	return ob_get_clean();
}

add_shortcode( 'ybp_button', 'ybp_button_shortcode' );

/*
 * YBP button Visual Composer Element
 */
$shortcode_fields = array(
		array(
			'type'          => 'textfield',
			'heading'       => esc_html__( 'Text', 'yourbutlerspantry' ),
			'param_name'    => 'ybp_text',
			'value'         => 'Learn More',
			'admin_label'   => true,
		),
		array(
			'type'          => 'vc_link',
			'heading'       => esc_html__( 'URL (Link)', 'yourbutlerspantry' ),
			'param_name'    => 'ybp_link',
			'value'         => '',
			'admin_label'   => true,
			'description'   => esc_html__( 'Add link to button.', 'yourbutlerspantry' ),
		),
		array(
			'type'          => 'dropdown',
			'param_name'    => 'ybp_alignment',
			'heading'       => esc_html__( 'Alignment', 'yourbutlerspantry' ),
			'value'    		=> array_flip( array(									
									'left'   => esc_html__( 'Left', 'yourbutlerspantry' ),
									'right'  => esc_html__( 'Right', 'yourbutlerspantry' ),
									'center' => esc_html__( 'Center', 'yourbutlerspantry' ),
								) ),
			'admin_label'   => true,
			'std'			=> 'left',
			'description'   => esc_html__( 'Select button alignment.', 'yourbutlerspantry' ),
		),
		array(
			'type'          => 'textfield',
			'heading'       => esc_html__( 'Extra class name', 'yourbutlerspantry' ),
			'param_name'    => 'ybp_class',
			'value'         => '',
			'description'   => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'yourbutlerspantry' ),
			'admin_label'   => true,
		),
	);

/*
 * Params
 */
$params = array(
	"name"                   	=> esc_html__( "Button", 'yourbutlerspantry' ),
	"description"            	=> esc_html__( "YBPbutton.", 'yourbutlerspantry' ),
	"base"                   	=> 'ybp_button',
	"class"                  	=> "ybp_element_wrapper",
	"controls"               	=> "full",
	"icon"                   	=> "",
	'category'               	=> esc_html__( 'YBP Addon', 'yourbutlerspantry' ),
	"show_settings_on_create"	=> true,
	"params"                 	=> $shortcode_fields,
);

vc_map( $params );