<?php
/*
 * Display Community Partners lists element
 */
function yourbutlerspantry_community_partners_shortcode( $atts, $content = null, $shortcode_handle = '' ) {
	$default_atts = array(
		'ybp_heading'   => '',	
	);

	$atts = shortcode_atts( $default_atts, $atts );

	extract($atts);
	ob_start();
	
	$cp_catarg   = array( 'orderby'  => 'name', 'order' => 'ASC', 'hide_empty' => true );
	$cp_category = get_terms( 'community-partners-category', $cp_catarg );
	echo '<div class="ybp-cp-list">';
	foreach ( $cp_category as $cpcat ) {
		
		$args = array(
				'post_type'      => 'ybpcommunitypartners',
				'post_status'    => 'publish',
				'orderby'        => 'id',
				'order'		     => 'asc',
				'tax_query'      => array(
					array(
						'taxonomy'  => 'community-partners-category',
						'field'     => 'term_id',
						'terms'     => $cpcat->term_id,
					),
				)
			);
		$query = new WP_Query( $args );
		
		echo '<div class="ybp-faq-wrapper">
				<h2>'.$cpcat->name.'</h2>
					<div class="ybp-vendor-list">';
		
		if ( $query->have_posts() ) {

			while ( $query->have_posts() ) {

				$query->the_post();
				$link = get_field('vendor_website_link');
				if(!empty($link)){
					$link_url = $link;
				}else{
					$link_url = 'javascript:void(0)';
				}
				?>
				<a href="<?php echo esc_url($link_url); ?>" target="_blank"><h3><?php echo esc_html(the_title()); ?></h3></a>
				<?php
			}
			
		}

			echo '</div>
		</div>';
	}
	echo '</div>';
	wp_reset_postdata();

	return ob_get_clean();
}

add_shortcode( 'ybp_community_partners', 'yourbutlerspantry_community_partners_shortcode' );

/*
 * Testimonial Composer Element
 */
$shortcode_fields = array(
		array(
			'type'            => 'textfield',
			'heading'         => esc_html__( 'Heading', 'yourbutlerspantry' ),
			'param_name'      => 'ybp_heading',
			'value'           => '',
			'description'     => esc_html__( 'Enter heading.', 'yourbutlerspantry' ),
			'admin_label'     => true,
		),		
	);

/*
 * Params
 */
$params = array(
	"name"                   	=> esc_html__( "Community Partners", 'yourbutlerspantry' ),
	"description"            	=> esc_html__( "Display Community Partners.", 'yourbutlerspantry' ),
	"base"                   	=> 'ybp_community_partners',
	"class"                  	=> "ybp_element_wrapper",
	"controls"               	=> "full",
	//"icon"                   	=> get_template_directory_uri() . '/images/faq.png',
	'category'               	=> esc_html__( 'YBP Addon', 'yourbutlerspantry' ),
	"show_settings_on_create"	=> true,
	"params"                 	=> $shortcode_fields,
);

vc_map( $params );