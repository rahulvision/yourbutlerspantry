<?php
/*
 * Display FAQ lists element
 */
function yourbutlerspantry_faq_shortcode( $atts, $content = null, $shortcode_handle = '' ) {
	$default_atts = array(
		'ybp_heading'   => '',
		'ybp_faq_cat'	=> '',
	);

	$atts = shortcode_atts( $default_atts, $atts );

	extract($atts);
	ob_start();
	
	$faq_catarg   = array( 'orderby'  => 'name', 'order' => 'ASC', 'hide_empty' => 0 );
	$faq_category = get_terms( 'ybp-faq-category', $faq_catarg );
	foreach ( $faq_category as $faqcat ) {
		
		$args = array(
				'post_type'      => 'ybpfaq',
				'post_status'    => 'publish',
				'orderby'        => 'id',
				'order'		     => 'asc',
				'tax_query'      => array(
					array(
						'taxonomy'  => 'ybp-faq-category',
						'field'     => 'term_id',
						'terms'     => $faqcat->term_id,
					),
				)
			);
		$query = new WP_Query( $args );
		
		echo '<div class="ybp-faq-wrapper">
				<h2>'.$faqcat->name.'</h2>
					<div class="faqaccordion">';
		
		if ( $query->have_posts() ) {

			while ( $query->have_posts() ) {

				$query->the_post();
				?>
				<h3><?php echo the_title(); ?></h3>
				<div>
					<p><?php echo get_the_content(); ?></p>
				</div>
				<?php
			}
				
		}

			echo '</div>
		</div>';
	}	
	wp_reset_postdata();

	return ob_get_clean();
}

add_shortcode( 'ybp_faq', 'yourbutlerspantry_faq_shortcode' );

/*
 * Testimonial Composer Element
 */
$shortcode_fields = array(
		array(
			'type'            => 'textfield',
			'heading'         => esc_html__( 'Heading', 'yourbutlerspantry' ),
			'param_name'      => 'ybp_heading',
			'value'           => '',
			'description'     => esc_html__( 'Enter heading.', 'yourbutlerspantry' ),
			'admin_label'     => true,
		),
		array(
			'type'            => 'autocomplete',
			'heading'         => esc_html__( 'Select FAQ Category', 'yourbutlerspantry' ),
			'param_name'      => 'ybp_faq_cat',
			'description'     => esc_html__( 'Type FAQ category name.', 'yourbutlerspantry' ),
			'settings'        => '',
			'admin_label'     => true,
		 ),
	);

/*
 * Params
 */
$params = array(
	"name"                   	=> esc_html__( "FAQ", 'yourbutlerspantry' ),
	"description"            	=> esc_html__( "Display FAQ.", 'yourbutlerspantry' ),
	"base"                   	=> 'ybp_faq',
	"class"                  	=> "ybp_element_wrapper",
	"controls"               	=> "full",
	"icon"                   	=> get_template_directory_uri() . '/images/faq.png',
	'category'               	=> esc_html__( 'YBP Addon', 'yourbutlerspantry' ),
	"show_settings_on_create"	=> true,
	"params"                 	=> $shortcode_fields,
);

vc_map( $params );