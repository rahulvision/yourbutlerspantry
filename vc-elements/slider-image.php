<?php
/*
 * display Slider Image element
 */
function yourbutlerspantry_slider_image_shortcode( $atts, $content = null, $shortcode_handle = '' ) {
	$default_atts = array(
		'slider_images'		=> '',
		'slider_caption'	=> '',
	);
	$atts = shortcode_atts( $default_atts, $atts );
	extract($atts);
	$slider_text_all = vc_param_group_parse_atts($slider_images);
	ob_start();
	?>
	<div class="header-slider-wrap">
		<div class="owl-carousel-slider owl-carousel owl-theme">

			<!-- Wrapper for slides -->
			<?php foreach ($slider_text_all as $slider_text): ?>
				<div class="item">
					<?php
					if (!empty($slider_text['image'])) {
						$image_src = wp_get_attachment_image_src($slider_text['image'], 'full');
					}
					?>
					<img src="<?php echo esc_url($image_src[0]);?>" >
					<?php
					if (!empty($slider_text['slider_caption'])) {
						echo '<span class="ybp-slider-caption">'.$slider_text['slider_caption'].'</span>';
					}
					?>
				</div>
			<?php endforeach; ?>

		</div>
		<div class="header-slider-icon-sec">			
			<div class="header-icon-section">
				<div class="header-icon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/footer-icon.png" /></div>				
			</div>
		</div>
	</div>
	<?php
	return ob_get_clean();
}

add_shortcode( 'ybp_slider_image', 'yourbutlerspantry_slider_image_shortcode' );

/*
 * Slider Image Visual Composer Element
 **/
$shortcode_fields = array(
	array(
		'type'       => 'param_group',
		"heading"     => esc_html__("Slider Images", 'yourbutlerspantry' ),
		'value'      => '',
		'param_name' => 'slider_images',
		'params'     => array(
			array(
				"type"        => "attach_image",
				"heading"     => esc_html__("Image", 'yourbutlerspantry' ),
				"param_name"  => "image",
				'admin_label' => true,
			),
			array(
				'type'            => 'textarea',
				'heading'         => esc_html__( 'Slider Caption', 'yourbutlerspantry' ),
				'param_name'      => 'slider_caption',
				'value'           => '',
				'description'     => esc_html__( 'Enter Slider Caption.', 'yourbutlerspantry' ),
				'admin_label'     => true,
			),
		),
		"group"     => esc_html__("Slider Images", 'yourbutlerspantry' ),
	),
);


// Params
$params = array(
	"name"                   	=> esc_html__( "Slider Image", 'yourbutlerspantry' ),
	"description"            	=> esc_html__( "Display Slider Image.", 'yourbutlerspantry' ),
	"base"                   	=> 'ybp_slider_image',
	"class"                  	=> "ybp_element_wrapper",
	"controls"               	=> "full",
	"icon"                   	=> get_template_directory_uri() . '/images/slider.png',
	'category'               	=> esc_html__( 'YBP Addon', 'yourbutlerspantry' ),
	"show_settings_on_create"	=> true,
	"params"                 	=> $shortcode_fields,
);

vc_map( $params );