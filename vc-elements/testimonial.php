<?php
/*
 * Display Testimonial lists element
 */
function yourbutlerspantry_testimonial_shortcode( $atts, $content = null, $shortcode_handle = '' ) {
	$default_atts = array(
		'ybp_heading'   => '',
		'ybp_post_type' => 'ybp-testimonial',
		'ybp_orderby'   => 'id',
		'ybp_order'     => 'asc',
		'ybp_itemno'	=> '3',
	);

	$atts = shortcode_atts( $default_atts, $atts );

	extract($atts);
	ob_start();

	$args = array(
			'post_type'      => $ybp_post_type,
			'orderby'        => $ybp_orderby,
			'order'		     => $ybp_order,
			'posts_per_page' => $ybp_itemno,
			'post_status'    => 'publish',
		);
	$query = new WP_Query( $args );
	$i = 1;
	if ( $query->have_posts() ) {

		echo '<div class="testimonial-items">';

			while ( $query->have_posts() ) {

				if ( $i == 1 ) {
					echo '<div class="testimonial-full">';
				} else {
					if ( $i % 2 == 0 ) {
						echo '<div class="testimonial-left">';
					} else {
						echo '<div class="testimonial-right">';
					}
				}
				$query->the_post();
				?>
					<div class="testimonial-desc">
						<p><?php echo get_the_content(); ?></p>
						<div class="testimonial-name">
							<p><?php echo get_field('full_name'); ?></p>
						</div>
					</div>
					
				</div>
				<?php
				$i++;
			}

		echo '</div>';

	}
	wp_reset_postdata();

	return ob_get_clean();
}

add_shortcode( 'ybp_testimonials', 'yourbutlerspantry_testimonial_shortcode' );

/*
 * Testimonial Composer Element
 */
$shortcode_fields = array(
		array(
			'type'            => 'textfield',
			'heading'         => esc_html__( 'Heading', 'yourbutlerspantry' ),
			'param_name'      => 'ybp_heading',
			'value'           => '',
			'description'     => esc_html__( 'Enter heading.', 'yourbutlerspantry' ),
			'admin_label'     => true,
		),
		array(
			'type'            => 'textfield',
			'heading'         => esc_html__( 'Total items', 'yourbutlerspantry' ),
			'param_name'      => 'ybp_itemno',
			'value'           => '3',
			'description'     => esc_html__( 'Set max limit for items in carousel or enter -1 to display all.', 'yourbutlerspantry' ),
			'admin_label'     => true,
		),
	);

/*
 * Params
 */
$params = array(
	"name"                   	=> esc_html__( "Testimonials", 'yourbutlerspantry' ),
	"description"            	=> esc_html__( "Display Testimonials.", 'yourbutlerspantry' ),
	"base"                   	=> 'ybp_testimonials',
	"class"                  	=> "ybp_element_wrapper",
	"controls"               	=> "full",
	"icon"                   	=> "",
	'category'               	=> esc_html__( 'YBP Addon', 'yourbutlerspantry' ),
	"show_settings_on_create"	=> true,
	"params"                 	=> $shortcode_fields,
);

vc_map( $params );