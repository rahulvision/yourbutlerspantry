<?php
/*
 * Display Our People lists element
 */
function yourbutlerspantry_our_people_shortcode( $atts, $content = null, $shortcode_handle = '' ) {
	$default_atts = array(
		'ybp_heading'   => '',
		'ybp_post_type' => 'ourpeople',
		'ybp_orderby'   => '',
		'ybp_order'     => '',
		'ybp_itemno'	   => '',
	);

	$atts = shortcode_atts( $default_atts, $atts );
	
	extract($atts);
	ob_start();
	
	$args = array(
			'post_type'      => $ybp_post_type,
			'orderby'        => $ybp_orderby,
			'order'		     => $ybp_order,
			'posts_per_page' => $ybp_itemno,
			'post_status'    => 'publish',
		);
	$query = new WP_Query( $args );
	
	if ( $query->have_posts() ) {
	
		echo '<div class="ourpeople">';
		
		while ( $query->have_posts() ) {
			$query->the_post();
			?>
				<div class="ourpeople-item">
					<?php if ( has_post_thumbnail() ) { ?>
						<div class="ourpeople-img">
							<?php the_post_thumbnail( 'ybp-our-people' ); ?>
						</div>
					<?php } ?>
					<div class="ourpeople-desc">
						<h2><?php esc_html(the_title()); ?></h2>
						<p>
						<?php echo $content = the_content();							
							// echo wp_trim_words( apply_filters('the_content', $content), 12, '...' ); 
						?>
						</p>
						<div class="ourpeople-email">
							<a href="mailto:<?php echo esc_attr(get_field('email')); ?>" ><?php echo get_field('email'); ?></a>
						</div>
						<div class="ourpeople-social">
							<ul class="ybp-social-media">
								<?php 
								$facebook_url = get_field('facebook_url');
								if ( $facebook_url ) {
									echo "<li><a href='".esc_url( $facebook_url )."' target='_blank' class='social-facebook'><i class='fab fa-facebook-f'></i></a></li>";
								}
								$instagram_url = get_field('instagram_url');
								if ( $instagram_url ) {
									echo "<li><a href='".esc_url( $instagram_url )."' target='_blank' class='social-instagram'><i class='fab fa-instagram'></i></a></li>";
								}
								$twitter_url = get_field('twitter_url');
								if ( $twitter_url ) {
									echo "<li><a href='".esc_url( $twitter_url )."' target='_blank' class='social-twitter'><i class='fab fa-twitter'></i></a></li>";
								}
								$linkedin_url = get_field('linkedin_url');
								if ( $linkedin_url ) {
									echo "<li><a href='".esc_url( $linkedin_url )."' target='_blank' class='social-linkedin'><i class='fab fa-linkedin-in'></i></a></li>";
								}
								$google_plus_url = get_field('google_plus_url');
								if ( $google_plus_url ) {
									echo "<li><a href='".esc_url( $google_plus_url )."' target='_blank' class='social-google-plus'><i class='fab fa-google-plus-g'></i></a></li>";
								}
								?>
							</ul>
						</div>
					</div>
				</div>
			<?php
		}
		echo '</div>';
	}
	wp_reset_postdata();

	return ob_get_clean();
}

add_shortcode( 'ybp_our_people', 'yourbutlerspantry_our_people_shortcode' );

/*
 * Our People Composer Element
 */
$shortcode_fields = array(
		array(
			'type'            => 'textfield',
			'heading'         => esc_html__( 'Heading', 'yourbutlerspantry' ),
			'param_name'      => 'ybp_heading',
			'value'           => '',
			'description'     => esc_html__( 'Enter heading.', 'yourbutlerspantry' ),
			'admin_label'     => true,
		),
		array(
			'type'            => 'dropdown',
			'param_name'      => 'ybp_orderby',
			'heading'         => esc_html__( 'Order by', 'yourbutlerspantry' ),
			'value'    		  => array_flip( array(
									'date'   => esc_html__( 'Date', 'yourbutlerspantry' ),
									'id'     => esc_html__( 'Order by post ID', 'yourbutlerspantry' ),
									'title'  => esc_html__( 'Title', 'yourbutlerspantry' ),
								) ),
			'admin_label'     => true,
			'description'     => esc_html__( 'Select order type.', 'yourbutlerspantry' ),
		),
		array(
			'type'            => 'dropdown',
			'param_name'      => 'ybp_order',
			'heading'         => esc_html__( 'Sort order', 'yourbutlerspantry' ),
			'value'    		  => array_flip( array(
									'desc' => esc_html__( 'Descending', 'yourbutlerspantry' ),
									'asc'  => esc_html__( 'Ascending', 'yourbutlerspantry' ),
								) ),
			'admin_label'     => true,
			'description'     => esc_html__( 'Select sorting order.', 'yourbutlerspantry' ),
		),
		array(
			'type'            => 'textfield',
			'heading'         => esc_html__( 'Total items', 'yourbutlerspantry' ),
			'param_name'      => 'ybp_itemno',
			'value'           => '3',
			'description'     => esc_html__( 'Set max limit for items in carousel or enter -1 to display all.', 'yourbutlerspantry' ),
			'admin_label'     => true,
		),
	);

/*
 * Params
 */
$params = array(
	"name"                   	=> esc_html__( "Our People", 'yourbutlerspantry' ),
	"description"            	=> esc_html__( "Display Our People List.", 'yourbutlerspantry' ),
	"base"                   	=> 'ybp_our_people',
	"class"                  	=> "ybp_element_wrapper",
	"controls"               	=> "full",
	"icon"                   	=> get_template_directory_uri() . '/images/people.png',
	'category'               	=> esc_html__( 'YBP Addon', 'yourbutlerspantry' ),
	"show_settings_on_create"	=> true,
	"params"                 	=> $shortcode_fields,
);

vc_map( $params );