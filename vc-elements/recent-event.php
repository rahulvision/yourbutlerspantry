<?php
/*
 * Display Our People lists element
 */
function yourbutlerspantry_recent_event_shortcode( $atts, $content = null, $shortcode_handle = '' ) {
	$default_atts = array(
		'ybp_heading'   => '',
		'ybp_post_type' => 'post',
		'ybp_orderby'   => '',
		'ybp_order'     => 'ASC',
		'ybp_itemno'	=> '3',
		'ybp_btn_text' 	=> '',
		'ybp_link'		=> '',
	);

	$atts = shortcode_atts( $default_atts, $atts );
	
	extract($atts);
	ob_start();
	
	$args = array(
			'post_type'      => $ybp_post_type,
			'orderby'        => $ybp_orderby,
			'order'		     => $ybp_order,
			'posts_per_page' => $ybp_itemno,
			'post_status'    => 'publish',		
		);
	$query = new WP_Query( $args );
	
	if ( $query->have_posts() ) {
	
		echo '<div class="ybp-recent-event">';
		
		while ( $query->have_posts() ) {
			$query->the_post();			
			?>
				<div class="post-content post-grid-item">
					<?php if ( has_post_thumbnail() ) { ?>
						<div class="post-image">
							<?php 
								the_post_thumbnail( 'ybp-our-people' ); 								
							?>
						</div>
					<?php } ?>	
					<div class="post-text">
						<?php
							the_title( '<h2 class="entry-title ybp-recent-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
							echo '<p class="ybp-recent-content">'.wp_trim_words( get_the_content(), 30, '...' ).'</p>';
						?>
						<div class="post-button">
							<a href="<?php echo esc_url( get_permalink() ); ?>" class="bpr-all-btn" ><?php echo esc_html( 'READ MORE', 'yourbutlerspantry' ); ?></a>
						</div>
						<div class="post-meta">
							<div class="post-date-time">
								<span class="date"><?php the_time('j') ?></span>
								<span class="month"><?php the_time('F') ?></span>
							</div>
						</div>
					</div>
				</div>
			<?php
		}
		echo '</div>';
		$ybp_link = vc_build_link($ybp_link);
		?>
			<div class="ybp-event-button">
				<a href="<?php echo esc_url($ybp_link['url']);?>" class="bpr-all-btn"><?php echo esc_html($ybp_btn_text);?></a>
			</div>
		<?php
	}
	wp_reset_postdata();

	return ob_get_clean();
}

add_shortcode( 'ybp_recent_event', 'yourbutlerspantry_recent_event_shortcode' );

/*
 * Our People Composer Element
 */
$shortcode_fields = array(
		array(
			'type'            => 'textfield',
			'heading'         => esc_html__( 'Heading', 'yourbutlerspantry' ),
			'param_name'      => 'ybp_heading',
			'value'           => '',
			'description'     => esc_html__( 'Enter heading.', 'yourbutlerspantry' ),
			'admin_label'     => true,
		),
		array(
			'type'            => 'textfield',
			'heading'         => esc_html__( 'Total items', 'yourbutlerspantry' ),
			'param_name'      => 'ybp_itemno',
			'value'           => '3',
			'description'     => esc_html__( 'Set max limit for items in carousel or enter -1 to display all.', 'yourbutlerspantry' ),
			'admin_label'     => true,
		),
		array(
			'type'            => 'textfield',
			'heading'         => esc_html__( 'Button Title', 'yourbutlerspantry' ),
			'param_name'      => 'ybp_btn_text',
			'value'           => '',
			'description'     => esc_html__( 'Button Text.', 'yourbutlerspantry' ),
			'admin_label'     => true,
		),
		array(
			'type'          => 'vc_link',
			'heading'       => esc_html__( 'URL (Link)', 'yourbutlerspantry' ),
			'param_name'    => 'ybp_link',
			'value'         => '',
			'admin_label'   => true,
			'description'   => esc_html__( 'Add link to button.', 'yourbutlerspantry' ),
		),
	);

/*
 * Params
 */
$params = array(
	"name"                   	=> esc_html__( "Recent Events", 'yourbutlerspantry' ),
	"description"            	=> esc_html__( "Display Recent Events.", 'yourbutlerspantry' ),
	"base"                   	=> 'ybp_recent_event',
	"class"                  	=> "ybp_element_wrapper",
	"controls"               	=> "full",
	//"icon"                   	=> get_template_directory_uri() . '/images/people.png',
	'category'               	=> esc_html__( 'YBP Addon', 'yourbutlerspantry' ),
	"show_settings_on_create"	=> true,
	"params"                 	=> $shortcode_fields,
);

vc_map( $params );