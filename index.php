<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package YourButlersPantry
 */

get_header();
?>

	<div id="primary" class="content-area container">
		<main id="main" class="site-main">

		<?php
		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) :
				?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
				<?php
			endif;
			?>
			<?php  $blogtitle = get_field('title', 'option');
				if ( !empty( $blogtitle ) && $blogtitle !="" ) { ?>
					<div class="ybp-blog-content">
						<h2><?php esc_html_e( get_field('title', 'option'), 'yourbutlerspantry' ); ?></h2>
						<hr></hr>
						<?php _e( get_field('blog_content', 'option'), 'yourbutlerspantry' ); ?>
					</div>
			<?php } ?>
			<div class="post-holder-wrap">
				<div id="post-masonry" class="blog-holder">
					<?php
						/* Start the Loop */
						while ( have_posts() ) :
							the_post();

							/*
							 * Include the Post-Type-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
							 */
							get_template_part( 'template-parts/content', get_post_type() );

						endwhile;

						//the_posts_navigation();

					else :

						get_template_part( 'template-parts/content', 'none' );

					endif;
					?>
				</div>
				<input type="submit" class="post-loadmore" value="<?php esc_html_e( 'Load More', 'yourbutlerspantry' ); ?>" >
			</div>
			<div class="post-sidebar"><?php get_sidebar(); ?></div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
