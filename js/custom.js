jQuery(document).ready(function(){
	//home page carousel slider
	setTimeout(function(){
		jQuery(".owl-carousel-slider").owlCarousel({
			items:1,
			loop:true,
			lazyLoad:true,
			autoplay:true,
			autoplayTimeout:5000,
			navigation: true,
			dots:true,
			pagination: true,
			margin:10,
		});
	}, 10);

	//sticky header
	if( jQuery( window ).width() >= 991 ) {
		jQuery(".header-menu").sticky({ topSpacing: 0 , zIndex: 999 });
	}

	//masonry grid for journal page
	var $container = jQuery('#post-masonry');
	$container.imagesLoaded(function () {
		$container.masonry({
			itemSelector: '.post-grid-item',
			isAnimated: true,
			animationOptions: {
				duration: 750,
				easing: 'linear',
				queue: false
			}
		});
	});

	//FAQ accordion
	jQuery( ".faqaccordion" ).accordion();

	//loadmore pagination
	jQuery('.post-loadmore').click(function() {

		var button = jQuery(this),
		    data = {
			'action': 'loadmorepost',
			'query': loadmore_params.posts,
			'page' : loadmore_params.current_page
		};
		jQuery.ajax({
			url : loadmore_params.ajaxurl,
			data : data,
			type : 'POST',
			beforeSend : function ( xhr ) {
				button.text('Loading...');
			},
			success : function( data ){
				if( data ) {
					//button.text( 'More posts' ).prev().before(data);
					button.text( 'More posts' );
					var items = jQuery( data );
					jQuery('#post-masonry').append( items ).masonry( 'appended', items );
					loadmore_params.current_page++;

					if ( loadmore_params.current_page == loadmore_params.max_page )
						button.remove();

				} else {
					button.remove();
				}
			}
		});
	});

	//mixup for rentals page
	jQuery( '.ybp-rentals-mixup' ).mixItUp({
        selectors: {
            target: '.ybp-rentals-list'
        }
	});

	//isotope for inspiration page
	var $cont = jQuery('.ybp-pinterest');
		$cont.imagesLoaded( function() {
		// Init isotope here after images have loaded..
		 $cont.isotope({
			filter: '*',
			animationOptions: {
				duration: 750,
				easing: 'linear',
				queue: false
			}
		});
	});
   
    /*$cont.isotope({
        filter: '*',
        animationOptions: {
            duration: 750,
            easing: 'linear',
            queue: false
        }
    });*/
    jQuery('.ybp-board').click(function(){
        jQuery('.ybp-board').removeClass('current');
        jQuery(this).addClass('current');
        var selector = jQuery(this).attr('data-filter');
        $cont.isotope({
            filter: selector,
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
         });
         return false;
    });
	jQuery(".fancybox").fancybox({
		openEffect	: 'none',
		closeEffect	: 'none',
		helpers		: {
			title	: { type : 'inside' }
		}
	});

	//matchHeight inspiration page
	jQuery('.title-holder').matchHeight();
	jQuery('.ybp-recent-title').matchHeight();
	jQuery('.ybp-recent-content').matchHeight();
	jQuery(".footer-top .col-3").matchHeight();

	//auto height footer col on load
	/*jQuery(window).resize(function() {
		if( jQuery( window ).width() >= 480 ) {
			var fheight = jQuery(".footer-top").height();
			jQuery(".footer-top .col-3").css('min-height',fheight);
		}
	});*/

	// Toggle Menu
	jQuery( '.header-menu .nav-menu li.menu-item-has-children .sub-menu' ).before(function() {
		return '<i class="ybp-angle-down"></i>';
	});

	jQuery('.header-menu .nav-menu li.menu-item-has-children').click(function() {
		var menu_focus  = jQuery( this ).hasClass('focus');
		if(menu_focus){
			jQuery('.header-menu .nav-menu li.menu-item-has-children .sub-menu').hide();
			jQuery( this ).removeClass('focus');
		}else{
			jQuery( this ).toggleClass( 'focus opened' );
		}
		var sub_menu = jQuery( this ).hasClass('focus opened');
		if( !sub_menu ) {
			jQuery('.header-menu .nav-menu li.menu-item-has-children .sub-menu').hide();
			jQuery( this ).removeClass('opened');
		} else {
			jQuery('.header-menu .nav-menu li.menu-item-has-children .sub-menu').show();
		}		
	});
});