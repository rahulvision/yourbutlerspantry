<?php
/**
 * YourButlersPantry functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package YourButlersPantry
 */

add_image_size( 'ybp-image-custom', 250, 250, true );
add_image_size( 'ybp-our-people', 425, 509, true );
add_image_size( 'ybp-rentals-img', 337, 343, true );


if ( ! function_exists( 'yourbutlerspantry_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function yourbutlerspantry_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on YourButlersPantry, use a find and replace
		 * to change 'yourbutlerspantry' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'yourbutlerspantry', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'yourbutlerspantry' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'yourbutlerspantry_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'yourbutlerspantry_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function yourbutlerspantry_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'yourbutlerspantry_content_width', 640 );
}
add_action( 'after_setup_theme', 'yourbutlerspantry_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function yourbutlerspantry_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'yourbutlerspantry' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'yourbutlerspantry' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Menu Social Media', 'yourbutlerspantry' ),
		'id'            => 'ybp-social-media',
		'description'   => esc_html__( 'Add widgets here.', 'yourbutlerspantry' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Column 1', 'yourbutlerspantry' ),
		'id'            => 'ybp-footer-col-1',
		'description'   => esc_html__( 'Add widgets here.', 'yourbutlerspantry' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Column 2', 'yourbutlerspantry' ),
		'id'            => 'ybp-footer-col-2',
		'description'   => esc_html__( 'Add widgets here.', 'yourbutlerspantry' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Column 3', 'yourbutlerspantry' ),
		'id'            => 'ybp-footer-col-3',
		'description'   => esc_html__( 'Add widgets here.', 'yourbutlerspantry' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	/*register_sidebar( array(
		'name'          => esc_html__( 'Footer Column 4', 'yourbutlerspantry' ),
		'id'            => 'ybp-footer-col-4',
		'description'   => esc_html__( 'Add widgets here.', 'yourbutlerspantry' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );*/
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Copyright', 'yourbutlerspantry' ),
		'id'            => 'ybp-footer-copyright',
		'description'   => esc_html__( 'Add widgets here.', 'yourbutlerspantry' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'yourbutlerspantry_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function yourbutlerspantry_scripts() {
	wp_enqueue_style( 'yourbutlerspantry-style', get_stylesheet_uri() );
	wp_enqueue_script( 'jquery-ui-accordion' );
	wp_enqueue_script( 'masonry' );
	wp_enqueue_script( 'imagesloaded' );
	wp_enqueue_script( 'yourbutlerspantry-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'yourbutlerspantry-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array(), '20151215', true );
	wp_enqueue_script( 'yourbutlerspantry-mixitup', get_template_directory_uri() . '/js/mixitup.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'yourbutlerspantry-isotope.pkgd.min', get_template_directory_uri() . '/js/isotope.pkgd.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'yourbutlerspantry-fancybox', get_template_directory_uri() . '/js/jquery.fancybox.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'yourbutlerspantry-matchheight', get_template_directory_uri() . '/js/jquery.matchHeight.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'yourbutlerspantry-custom', get_template_directory_uri() . '/js/custom.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'yourbutlerspantry-sticky', get_template_directory_uri() . '/js/jquery.sticky.js', array(), '20151215', true );
	wp_enqueue_script( 'yourbutlerspantry-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	Global $wp_query;
	wp_localize_script( 'yourbutlerspantry-custom', 'loadmore_params', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'posts' => json_encode( $wp_query->query_vars ),
		'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
		'max_page' => $wp_query->max_num_pages
	) );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_enqueue_style( 'yourbutlerspantry-carousel', get_template_directory_uri() . '/css/owl.carousel.min.css' );
	wp_enqueue_style( 'yourbutlerspantry-fancybox', get_template_directory_uri() . '/css/jquery.fancybox.css' );
	wp_enqueue_style( 'your-butlers-pantry-fontawesome', get_template_directory_uri() . '/css/fontawesome.min.css' );
	wp_enqueue_style( 'yourbutlerspantry-fontawesome-all', get_template_directory_uri() . '/css/all.min.css' );
	wp_enqueue_style( 'yourbutlerspantry-responsive', get_template_directory_uri() . '/css/responsive.css' );
}
add_action( 'wp_enqueue_scripts', 'yourbutlerspantry_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Include custom post type.
 */
require get_template_directory() . '/inc/custom-post-type.php';

/**
 * Include social media widget.
 */
require get_template_directory() . '/widget/social-media-widget.php';

/**
 * Include contactus widget.
 */
require get_template_directory() . '/widget/contact-widget.php';

/**
 * Include VC elements.
 */
add_action( 'init', 'yourbutlerspantry_vc_shortcode' );
function yourbutlerspantry_vc_shortcode() {

	if ( function_exists('is_plugin_active') && is_plugin_active( 'js_composer/js_composer.php' ) ) {

		require get_template_directory() . '/vc-elements/slider-image.php';
		require get_template_directory() . '/vc-elements/our-people-post.php';
		require get_template_directory() . '/vc-elements/testimonial.php';
		require get_template_directory() . '/vc-elements/faq.php';
		require get_template_directory() . '/vc-elements/ybp-button.php';
		require get_template_directory() . '/vc-elements/pinterest.php';
		require get_template_directory() . '/vc-elements/community-partners.php';
		require get_template_directory() . '/vc-elements/recent-event.php';
	}
}

/**
 * loadmore_posts.
 */
add_action('wp_ajax_loadmorepost', 'yourbutlerspantry_loadmore_post');
add_action('wp_ajax_nopriv_loadmorepost', 'yourbutlerspantry_loadmore_post');
function yourbutlerspantry_loadmore_post(){
	$args = json_decode( stripslashes( $_POST['query'] ), true );
	$args['paged'] = $_POST['page'] + 1;
	$args['post_status'] = 'publish';

	query_posts( $args );

	if( have_posts() ) :

		while( have_posts() ): the_post();

			get_template_part( 'template-parts/content', get_post_type() );

		endwhile;

	endif;
	wp_die();
}

/**
 * loadmore_posts.
 */
add_filter( 'post_row_actions', 'yourbutlerspantry_remove_row_actions', 10, 1 );
function yourbutlerspantry_remove_row_actions( $actions ) {
    if( get_post_type() === 'ourpeople' ||
		get_post_type() === 'ybp_testimonial' ||
		get_post_type() === 'ybprentals' ||
		get_post_type() === 'ybpfaq' ||
		get_post_type() === 'ybpcommunitypartners'
	)
        unset( $actions['view'] );
    return $actions;
}

/**
 * Display ACF Theme option page
 */
if( function_exists('acf_add_options_page') ) {

	$parent = acf_add_options_page(array(
		'page_title' 	=> 'Theme General Options',
		'menu_title'	=> 'Theme Options',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	// add blog page
	acf_add_options_sub_page(array(
		'page_title'  => 'Blog Content',
		'menu_title'  => 'Blog Content',
		'parent_slug'  => $parent['menu_slug'],
	));

}

/**
 * Exclude pages from WordPress Search
 */
function yourbutlerspantry_search_filter($query) {
	if ($query->is_search) {
		$query->set('post_type', 'post');
	}
	return $query;
}
add_filter('pre_get_posts','yourbutlerspantry_search_filter');